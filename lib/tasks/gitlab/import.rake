def get_group_id(group_path, parent_id)
  group_name, remain = group_path.split('/', 2);
  #top group, should end
  group_id = get_group_id_helper(group_name, parent_id);
  unless remain
    return group_id
  else
    return get_group_id(remain, group_id)
  end
end

def get_group_id_helper(group_name, parent_id)
  user = User.admins.reorder("id").first
  if group_name
    group = Namespace.find_by(path: group_name)
    # create group namespace
    unless group
      group = Group.new(name: group_name)
      group.path = group_name
      group.owner = user
      if parent_id
        group.parent_id = parent_id
      end
      if group.save
        puts " * Created Group #{group.name} (#{group.id})".color(:green)
      else
        puts " * Failed trying to create group #{group.name}".color(:red)
      end
    end
    # set project group
    return group.id
  end
end

namespace :gitlab do
  namespace :import do
    # How to use:
    #
    #  1. copy the bare repos under the repository storage paths (commonly the default path is /home/git/repositories)
    #  2. run: bundle exec rake gitlab:import:repos RAILS_ENV=production
    #
    # Notes:
    #  * The project owner will set to the first administator of the system
    #  * Existing projects will be skipped
    #
    desc "GitLab | Import bare repositories from repositories -> storages into GitLab project instance"
    task repos: :environment do
      Gitlab.config.repositories.storages.each_value do |repository_storage|
        git_base_path = repository_storage['path']
        repos_to_import = Dir.glob(git_base_path + '/**/*.git')

        repos_to_import.each do |repo_path|
          # strip repo base path
          repo_path[0..git_base_path.length] = ''

          path = repo_path.sub(/\.git$/, '')
          group_path, name = File.split(path)
          group_path= nil if group_path == '.'

          puts "Processing #{repo_path}".color(:yellow)

          if path.end_with?('.wiki')
            puts " * Skipping wiki repo"
            next
          end

          project = Project.find_by_full_path(path)

          if project
            puts " * #{project.name} (#{repo_path}) exists"
          else
            user = User.admins.reorder("id").first

            project_params = {
              name: name,
              path: name
            }

            # find group namespace
            # group name can be a full path a/b
            if group_path
              project_params[:namespace_id] = get_group_id(group_path, nil)
            end

            project = Projects::CreateService.new(user, project_params).execute

            if project.persisted?
              puts " * Created #{project.name} (#{repo_path})".color(:green)
              ProjectCacheWorker.perform_async(project.id)
            else
              puts " * Failed trying to create #{project.name} (#{repo_path})".color(:red)
              puts "   Errors: #{project.errors.messages}".color(:red)
            end
          end
        end
      end

      puts "Done!".color(:green)
    end
  end
end
